public class Point {
    int xCoord;
    int yCoord;

    public Point(int xCoord,int yCoord){
        this.xCoord=xCoord;
        this.yCoord=yCoord;
    }

    public double DistanceFromAPoint(Point point){
        int xDiffe=xCoord - point.xCoord;
        int yDiffe = yCoord - point.yCoord;
        return Math.sqrt((xDiffe*xDiffe) +(yDiffe*yDiffe));

    }



}
